FROM node:10

RUN yarn global add @vue/cli @vue/cli-service-global firebase-tools

# make the 'app' folder the current working directory
WORKDIR /code

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package.json yarn.lock ./

RUN yarn install

EXPOSE 8080
EXPOSE 9005
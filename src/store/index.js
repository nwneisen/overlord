import Vue from 'vue'
import Vuex from 'vuex'
import firebase from './modules/firebase'
import timer from './modules/timer'
import spotify from './modules/spotify'
import twitch from './modules/twitch'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    firebase,
    timer,
    spotify,
    twitch
  },
  state: { // = data
  },
  getters: { // = computed properties
  },
  mutations: { // = setting and updating state
  },
  actions: { // = methods
  },
})
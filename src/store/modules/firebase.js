import firebase from 'firebase'

export default ({
  state: { // = data
    email: '',
    password: '',
    newPassword: '',
    authUser: null,
    displayName: null,
    photoURL: null,
    favoriteFood: null
  },
  getters: { // = computed properties
    authUser: function (state) {
      return state.authUser;
    },
    displayName: function (state) {
      return state.displayName;
    },
    photoURL: function (state) {
      return state.photoURL;
    },
    linkedPAssword: function (state) {
      if (state.authUser) {
        return !!state.authUser.providerData.find(provider => provider.providerId === 'password')
      }
    },
    linkedGoogle: function (state) {
      if (state.authUser) {
        return !!state.authUser.providerData.find(provider => provider.providerId === 'google.com')
      }
    },
  },
  mutations: { // = setting and updating state
    setEmail: function (state, email) {
      state.email = email;
    },
    setPassword: function (state, password) {
      state.password = password;
    },
    setNewPassword: function (state, newPassword) {
      state.newPassword = newPassword;
    },
    setAuthUser: function (state, authUser) {
      state.authUser = authUser;
    },
    setDisplayName: function (state, displayName) {
      state.displayName = displayName;
    },
    setPhotoURL: function (state, photoURL) {
      state.photoURL = photoURL;
    },
    setFavoriteFood: function (state, favoriteFood) {
      state.favoriteFood = favoriteFood;
    },
  },
  actions: { // = methods
    setUser: function (context) {
      firebase.auth().onAuthStateChanged(user => {
        context.commit('setAuthUser', user);
        if (user) {
          context.commit('setDisplayName', user.displayName);
          context.commit('setPhotoURL', user.photoURL);
          context.commit('setEmail', user.email);
          firebase.database().ref('users').child(user.uid).once('value', snapshot => {
            if (snapshot.val()) {
              context.commit('setFavoriteFood', snapshot.val().favoriteFood);
            }
          })
        }
      })
    },
    signIn: function (context, payload) {
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(() => console.log("Successfully signed in with email"))
        .catch(error => console.log(error.message));
    },
    signInWithGoogle: function () {
      const provider = new firebase.auth.GoogleAuthProvider();
      firebase.auth().signInWithPopup(provider)
      .then(() => console.log("Successfully signed in with google"))
        .catch(error => console.log(error.message));
    },
    register: function (context, payload) {
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
      .then(() => console.log("Successfully created user with email"))
        .catch(error => console.log(error.message));
    },
    signOut: function () {
      firebase.auth().signOut();
    }
  },
})
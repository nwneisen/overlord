import axios from 'axios'

export default ({
  state: { // = data
    song: "",
    artist: ""
  },
  getters: { // = computed properties
    nowPlaying: function(state) {
      return " " + state.song + " by " + state.artist;
    },
  },
  mutations: { // = setting and updating state
    setSong: function(state, song) {
      state.song = song;
    },
    setArtist: function(state, artist) {
      state.artist = artist;
    }
  },
  actions: { // = methods
    initSpotify: function (context) {
      if(!context.interval) {
        context.dispatch('getCurrentSong');
        context.interval = setInterval(() => {
          context.dispatch('getCurrentSong')
        }, 60 * 1000)
      }
    },
    getCurrentSong: function(context) {
      console.log("Getting current song")
      var authOptions = {
        method: "GET",
        url: "https://api.spotify.com/v1/me/player/currently-playing",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Authorization": "Bearer BQCpazBHzGuX_TzetTSIKIxWq3ULK_cymUPBlqAR5nERXIoCbr0rnQBGUvYYPl7nhx9SYv_r9Vkpf4v8FqcUB7vAC2S-T17q4DKWfVbSCj8NirLRfEuUPSI4PGBh-OC_PWo1ChZiq98Y5WH5"
        }
      };
      axios(authOptions)
        .then(response => {
          context.commit('setSong', response.data.item.name);
          context.commit('setArtist',  response.data.item.artists[0].name);
        })
        .catch(error => {
          console.log(error.response);
        });
    }
  },
})
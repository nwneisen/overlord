import axios from 'axios'
import tmi from 'tmi.js'

export default ({
  state: { // = data
    client: null,
    opts: {
      identity: {
        username: "neizn",
        password: "live_90782115_3damlD4lt50b7piuAqg60XnulrfwyI"
      },
      channels: [
        "neizn"
      ]
    },
    messages: []
  },
  getters: { // = computed properties
    messages: function (state) {
      return state.messages;
    },
  },
  mutations: { // = setting and updating state
    setMessages: function(state) {
      state.messages = messages;
    },
    onConnectedHandler: function (state, addr, port) {
      console.log(`* Connected to ${addr}:${port}`);
    }
  },
  actions: { // = methods
    initTwitch: function (context) {
      if(!context.client) {
        context.client = new tmi.client(context.opts);
        // context.client.on('message', onMessageHandler);
        context.client.on('connected', () => {context.commit("onConnectedHandler")});
        context.client.connect();
      }
    },
  },
})
import { dispatch } from "rxjs/internal/observable/pairs";

export default ({
  state: { // = data
    state: "paused",
    startTime: Date.now(),
    currentTime: Date.now(),
    interval: null
  },
  getters: { // = computed properties
    time: function(state, getters) {
      return getters.hours + ":" + getters.minutes + ":" + getters.seconds;
    },
    milliseconds: function(state, getters) {
      return state.currentTime - state.startTime;
    },
    hours: function(state, getters) {
      var lapsed = getters.milliseconds;
      var hrs = Math.floor(lapsed / 1000 / 60 / 60);
      return hrs >= 10 ? hrs : "0" + hrs;
    },
    minutes: function(state, getters) {
      var lapsed = getters.milliseconds;
      var min = Math.floor((lapsed / 1000 / 60) % 60);
      return min >= 10 ? min : "0" + min;
    },
    seconds: function(state, getters) {
      var lapsed = getters.milliseconds;
      var sec = Math.ceil((lapsed / 1000) % 60);
      return sec >= 10 ? sec : "0" + sec;
    }
  },
  mutations: { // = setting and updating state
    updateCurrentTime: function(state) {
      if (state.state === "started") {
        state.currentTime = Date.now();
      }
    },
    reset: function(state) {
      state.state = "started";
      state.startTime = Date.now();
      state.currentTime = Date.now();
    },
    pause: function(state) {
      state.state = "paused";
    }
  },
  actions: { // = methods
    initTimer: function (context) {
      if(!context.interval) {
        context.interval = setInterval(() => {
          context.commit('updateCurrentTime')
        }, 1000)
      }
    },
  },
})
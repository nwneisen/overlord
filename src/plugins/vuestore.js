import Vue from "vue";
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
Vue.use(VueFire)
firebase.initializeApp({
  apiKey: "AIzaSyC_dU_e0CFezdPQYFyvcFWgJzT8mUlhh5U",
  authDomain: "overlord-b0198.firebaseapp.com",
  databaseURL: "https://overlord-b0198.firebaseio.com",
  projectId: "overlord-b0198",
  storageBucket: "overlord-b0198.appspot.com",
  messagingSenderId: "876344929591"
})
export const db = firebase.firestore()
const firestore = firebase.firestore()
const settings = {};
firestore.settings(settings);

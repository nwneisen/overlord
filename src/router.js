import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home";
import Overlay from "@/views/Overlay";
import Editor from "@/views/Editor";
import Settings from "@/views/Settings";
import Login from "@/views/Login";
import Logout from "@/views/Logout";
import Register from "@/views/Register";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/overlay",
      name: "overlay",
      component: Overlay
    },
    {
      path: "/editor",
      name: "editor",
      component: Editor
    },
    {
      path: "/settings",
      name: "settings",
      component: Settings
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/logout",
      name: "logout",
      component: Logout,
    },
    {
      path: "*",
      redirect: "/"
    },
  ]
});
